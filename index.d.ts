import { TokenPayloadDto } from 'src/modules/auth/dtos/token-payload.dto';

export {};

declare global {
  namespace Express {
    export interface Request {
      tokenPayload: TokenPayloadDto;
    }
  }
}
