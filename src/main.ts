import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { ApiTag } from './constants/api-tag.constant';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      skipMissingProperties: false,
    }),
  );

  initSwagger(app);

  await app.listen(3000);
}

function initSwagger(app: INestApplication) {
  const config = new DocumentBuilder()
    .setTitle('Niko Niko Api')
    .setDescription('Niko Niko Api Specification')
    .addTag(ApiTag.AUTH)
    .addTag(ApiTag.SCORES)
    .addSecurity('bearer', {
      type: 'http',
      scheme: 'bearer',
    })
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('spec', app, document);
}

bootstrap();
