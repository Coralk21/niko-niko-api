import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { Request } from 'express';
import { AppConfig } from 'src/constants/app-config.constant';
import { TokenDto } from 'src/modules/auth/dtos/token.dto';
import { TokenService } from 'src/modules/auth/services/token/token.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private tokenService: TokenService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest<Request>();
    const token = this.extractTokenFromHeader(request);

    if (!token) throw new UnauthorizedException();

    try {
      const payload = await this.tokenService.getPayload(token);
      request.tokenPayload = payload;
    } catch {
      throw new UnauthorizedException();
    }
    return true;
  }

  extractTokenFromHeader(request: Request): TokenDto | undefined {
    const [type, token] = request.headers.authorization?.split(' ') ?? [];
    return type === AppConfig.JWT_TOKEN_TYPE ? { type, token } : undefined;
  }
}
