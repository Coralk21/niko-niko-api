import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from '@nestjs/typeorm';
import { EnvironmentKey } from 'src/constants/environment-key.constant';
import { Profile } from 'src/entities/profile.entity';
import { Score } from 'src/entities/score.entity';
import { User } from 'src/entities/user.entity';

@Injectable()
export class TypeOrmOptionsService implements TypeOrmOptionsFactory {
  constructor(private configService: ConfigService) {}

  createTypeOrmOptions(): TypeOrmModuleOptions {
    return {
      type: this.configService.getOrThrow<'postgres'>(EnvironmentKey.DB_TYPE),
      host: this.configService.getOrThrow(EnvironmentKey.DB_HOST),
      port: this.configService.getOrThrow<number>(EnvironmentKey.DB_PORT),
      username: this.configService.getOrThrow(EnvironmentKey.DB_USERNAME),
      password: this.configService.getOrThrow<string>(
        EnvironmentKey.DB_PASSWORD,
      ),
      database: this.configService.getOrThrow<string>(EnvironmentKey.DB_NAME),
      entities: [Profile, Score, User],
      synchronize: true,
    };
  }
}
