import { ConflictException, Injectable } from '@nestjs/common';
import { DataSource, Repository } from 'typeorm';
import { NewUserDto } from '../../dtos/new-user.dto';
import { User } from 'src/entities/user.entity';
import { Profile } from 'src/entities/profile.entity';
import { DBErrorCode } from 'src/constants/db-error-code.constant';
import { encryptPassword } from 'src/helpers/password.helper';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    private dataSource: DataSource,
  ) {}

  async createUser(userData: NewUserDto): Promise<User> {
    const queryRunner = this.dataSource.createQueryRunner();

    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      const hashedPassword = await encryptPassword(userData.password);
      const user = await queryRunner.manager.save(
        new User(userData.username, hashedPassword),
      );
      await queryRunner.manager.save(new Profile(userData.name, user));
      await queryRunner.commitTransaction();
      return user;
    } catch (err) {
      await queryRunner.rollbackTransaction();
      if (err.code === DBErrorCode.CONFLICT) {
        throw new ConflictException();
      }
    } finally {
      await queryRunner.release();
    }
  }

  findUserByUsername(username: string): Promise<User | null> {
    return this.usersRepository.findOne({
      where: {
        username,
      },
    });
  }
}
