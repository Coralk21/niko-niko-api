import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, MaxLength } from 'class-validator';

export class NewUserDto {
  @ApiProperty({ description: 'User fullname', example: 'Andres Perez' })
  @IsString()
  @IsNotEmpty()
  @MaxLength(128)
  name: string;

  @ApiProperty({ description: 'Username', example: 'andres002' })
  @IsString()
  @IsNotEmpty()
  @MaxLength(255)
  username: string;

  @ApiProperty({ description: 'Password', example: 'supersecret' })
  @IsString()
  @IsNotEmpty()
  password: string;
}
