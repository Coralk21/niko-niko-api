import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Score } from 'src/entities/score.entity';
import { Repository } from 'typeorm';
import { NewScoreDto } from '../../dtos/new-score.dto';
import { User } from 'src/entities/user.entity';
import { And, MoreThan, LessThanOrEqual } from 'typeorm';

@Injectable()
export class ScoreService {
  constructor(
    @InjectRepository(Score) private scoresRepository: Repository<Score>,
    @InjectRepository(User) private usersRepository: Repository<User>,
  ) {}

  async createScore(data: NewScoreDto, userId: number): Promise<Score> {
    const user = this.usersRepository.create({ id: userId });
    const score = this.scoresRepository.create({ ...data, user });
    await this.scoresRepository.insert(score);
    return score;
  }

  async getOneByDateRange(
    start: Date,
    end: Date,
    userId: number,
  ): Promise<Score | null> {
    return this.scoresRepository.findOne({
      where: {
        user: {
          id: userId,
        },
        created_at: And(MoreThan(start), LessThanOrEqual(end)),
      },
    });
  }

  async getByDateRange(
    start: Date,
    end: Date,
    userId: number,
  ): Promise<Score[]> {
    return this.scoresRepository.find({
      where: {
        user: {
          id: userId,
        },
        created_at: And(MoreThan(start), LessThanOrEqual(end)),
      },
      order: {
        created_at: 'ASC',
      },
    });
  }
}
