import {
  Body,
  Controller,
  Get,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { ApiPath } from 'src/constants/api-path.constant';
import { NewScoreDto } from '../../dtos/new-score.dto';
import { Request } from 'express';
import { ScoreService } from '../../services/score/score.service';
import { AuthGuard } from 'src/guards/auth/auth.guard';
import { getDayEnd, getDayStart } from 'src/helpers/date.helper';
import { DateRangeDto } from '../../dtos/date-range.dto';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiSecurity,
  ApiTags,
} from '@nestjs/swagger';
import { ApiTag } from 'src/constants/api-tag.constant';

@ApiBearerAuth()
// @ApiSecurity('bearer')
@ApiResponse({ status: 401, description: 'Not authorized' })
@ApiTags(ApiTag.SCORES)
@UseGuards(AuthGuard)
@Controller(ApiPath.SCORES)
export class ScoreController {
  constructor(private scoreService: ScoreService) {}

  @ApiOperation({ summary: 'Create new score' })
  @ApiResponse({ status: 200, description: 'Score created' })
  @Post()
  createScore(@Body() newScore: NewScoreDto, @Req() { tokenPayload }: Request) {
    return this.scoreService.createScore(newScore, tokenPayload.sub);
  }

  @ApiOperation({ summary: 'Get score for current day' })
  @ApiResponse({ status: 200 })
  @Get(ApiPath.CURRENT)
  getCurrentScore(@Req() { tokenPayload }: Request) {
    const today = new Date();
    const startDay = getDayStart(today);
    const endDay = getDayEnd(today);
    return this.scoreService.getOneByDateRange(
      startDay,
      endDay,
      tokenPayload.sub,
    );
  }

  @ApiOperation({ summary: 'Get scores by date range' })
  @ApiResponse({ status: 200 })
  @ApiQuery({ name: 'startDate', example: '2023-08-01' })
  @ApiQuery({ name: 'endDate', example: '2023-08-31' })
  @Get()
  getByDateRange(
    @Query() range: DateRangeDto,
    @Req() { tokenPayload }: Request,
  ) {
    const startDate = getDayStart(range.startDate);
    const endDate = getDayEnd(range.endDate);
    return this.scoreService.getByDateRange(
      startDate,
      endDate,
      tokenPayload.sub,
    );
  }
}
