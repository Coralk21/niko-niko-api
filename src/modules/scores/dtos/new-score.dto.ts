import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, IsString, Max, MaxLength, Min } from 'class-validator';

export class NewScoreDto {
  @ApiProperty({ description: 'Score', example: 4 })
  @IsNumber()
  @Min(1)
  @Max(4)
  score: number;

  @ApiProperty({ description: 'Comment', example: 'Good day' })
  @IsString()
  @MaxLength(512)
  comment: string;
}
