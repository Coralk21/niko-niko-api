import { Module } from '@nestjs/common';
import { ScoreController } from './controllers/score/score.controller';
import { ScoreService } from './services/score/score.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Score } from 'src/entities/score.entity';
import { User } from 'src/entities/user.entity';
import { AuthModule } from '../auth/auth.module';

@Module({
  imports: [TypeOrmModule.forFeature([Score, User]), AuthModule],
  controllers: [ScoreController],
  providers: [ScoreService],
})
export class ScoresModule {}
