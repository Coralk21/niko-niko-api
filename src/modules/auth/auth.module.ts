import { Module } from '@nestjs/common';
import { AuthController } from './controllers/auth/auth.controller';
import { UsersModule } from '../users/users.module';
import { TokenService } from './services/token/token.service';
import { JwtModule } from '@nestjs/jwt';
import { JwtOptionsService } from './services/jwt-options/jwt-options.service';

@Module({
  imports: [
    JwtModule.registerAsync({
      useClass: JwtOptionsService,
    }),
    UsersModule,
  ],
  controllers: [AuthController],
  providers: [TokenService],
  exports: [TokenService],
})
export class AuthModule {}
