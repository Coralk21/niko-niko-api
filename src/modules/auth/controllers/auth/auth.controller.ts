import { Body, Controller, Post, UnauthorizedException } from '@nestjs/common';
import { ApiPath } from 'src/constants/api-path.constant';
import { NewUserDto } from 'src/modules/users/dtos/new-user.dto';
import { UserService } from 'src/modules/users/services/user/user.service';
import { UserCredentialsDto } from '../../dtos/user-credentials.dto';
import { validatePassword } from 'src/helpers/password.helper';
import { TokenService } from '../../services/token/token.service';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { ApiTag } from 'src/constants/api-tag.constant';

@ApiTags(ApiTag.AUTH)
@Controller(ApiPath.AUTH)
export class AuthController {
  constructor(
    private userService: UserService,
    private tokenService: TokenService,
  ) {}

  @ApiOperation({ summary: 'Register new user' })
  @ApiResponse({ status: 201, description: 'User created' })
  @ApiResponse({ status: 409, description: 'Confict with username' })
  @Post(ApiPath.REGISTER)
  async register(@Body() newUser: NewUserDto) {
    const user = await this.userService.createUser(newUser);
    return { id: user.id };
  }

  @ApiOperation({ summary: 'Login user' })
  @ApiResponse({ status: 200, description: 'Login successful' })
  @ApiResponse({ status: 401, description: 'Not authorized' })
  @Post(ApiPath.LOGIN)
  async login(@Body() credentials: UserCredentialsDto) {
    const user = await this.userService.findUserByUsername(
      credentials.username,
    );
    if (!user) throw new UnauthorizedException();
    const isValidPassword = await validatePassword(
      credentials.password,
      user.password,
    );
    if (!isValidPassword) throw new UnauthorizedException();
    return this.tokenService.createToken({
      sub: user.id,
    });
  }
}
