import { Test, TestingModule } from '@nestjs/testing';
import { JwtOptionsService } from './jwt-options.service';

describe('JwtOptionsService', () => {
  let service: JwtOptionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [JwtOptionsService],
    }).compile();

    service = module.get<JwtOptionsService>(JwtOptionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
