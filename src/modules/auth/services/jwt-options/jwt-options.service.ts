import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModuleOptions, JwtOptionsFactory } from '@nestjs/jwt';
import { AppConfig } from 'src/constants/app-config.constant';
import { EnvironmentKey } from 'src/constants/environment-key.constant';
import { base64ToUTF8 } from 'src/helpers/base64.helper';

@Injectable()
export class JwtOptionsService implements JwtOptionsFactory {
  constructor(private configService: ConfigService) {}

  createJwtOptions(): JwtModuleOptions {
    const privateKey = base64ToUTF8(
      this.configService.getOrThrow(EnvironmentKey.JWT_PRIVATE_KEY),
    );
    const publicKey = base64ToUTF8(
      this.configService.getOrThrow(EnvironmentKey.JWT_PUBLIC_KEY),
    );
    return {
      privateKey,
      publicKey,
      signOptions: {
        algorithm: AppConfig.JWT_SIGN_ALGORITHM,
        expiresIn: AppConfig.JWT_EXPIRES_IN,
      },
    };
  }
}
