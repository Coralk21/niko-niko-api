import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { TokenPayloadDto } from '../../dtos/token-payload.dto';
import { TokenDto } from '../../dtos/token.dto';
import { AppConfig } from 'src/constants/app-config.constant';

@Injectable()
export class TokenService {
  constructor(private jwtService: JwtService) {}

  async createToken(payload: TokenPayloadDto): Promise<TokenDto> {
    return {
      type: AppConfig.JWT_TOKEN_TYPE,
      token: await this.jwtService.signAsync(payload),
    };
  }

  async getPayload(token: TokenDto): Promise<TokenPayloadDto> {
    return await this.jwtService.verifyAsync(token.token);
  }
}
