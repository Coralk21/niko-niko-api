import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class UserCredentialsDto {
  @ApiProperty({ description: 'Username', example: 'andres002' })
  @IsString()
  @IsNotEmpty()
  username: string;

  @ApiProperty({ description: 'Password', example: 'supersecret' })
  @IsString()
  @IsNotEmpty()
  password: string;
}
