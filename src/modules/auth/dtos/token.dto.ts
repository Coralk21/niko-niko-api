export interface TokenDto {
  type: string;
  token: string;
}
