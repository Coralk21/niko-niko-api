export class ApiPath {
  static readonly AUTH = 'auth';
  static readonly REGISTER = 'register';
  static readonly LOGIN = 'login';
  static readonly SCORES = 'scores';
  static readonly CURRENT = 'current';
}
