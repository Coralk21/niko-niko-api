export class AppConfig {
  static readonly PASSWORD_SALTS = 10;
  static readonly JWT_SIGN_ALGORITHM = 'RS256';
  static readonly JWT_EXPIRES_IN = '1d';
  static readonly JWT_TOKEN_TYPE = 'Bearer';
}
