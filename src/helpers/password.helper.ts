import { hash, compare } from 'bcrypt';
import { AppConfig } from 'src/constants/app-config.constant';

export function encryptPassword(password: string): Promise<string> {
  return hash(password, AppConfig.PASSWORD_SALTS);
}

export function validatePassword(
  password: string,
  encrypted: string,
): Promise<boolean> {
  return compare(password, encrypted);
}
