import * as moment from 'moment';

export function getDayStart(date: Date | string): Date {
  const momentDate = moment(date);
  momentDate.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
  return momentDate.toDate();
}

export function getDayEnd(date: Date | string): Date {
  const momentDate = moment(date);
  momentDate.set({ hour: 23, minute: 59, second: 59, millisecond: 999 });
  return momentDate.toDate();
}
